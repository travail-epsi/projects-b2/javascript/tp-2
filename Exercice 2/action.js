$(document).ready(function () {
        let joueurEnCours, scoreX = 0, scoreO = 0;
        const joueurO = "O";
        const joueurX = "X";
        let play = true;

        function randomPlayer() {
            let piece = Math.floor(Math.random() * Math.floor(2));
            if (piece > 0) {
                joueurEnCours = joueurX;
            } else {
                joueurEnCours = joueurO;
            }

        }

        if (!joueurEnCours) {
            randomPlayer()
        }
        $('.case').click(onClick);

        function onClick() {
            if (!$(this).html()) {
                if (play) {
                    $(this).html(joueurEnCours);
                    let result = checkWin(joueurEnCours);
                    if (!result) {
                        joueurEnCours = changeJoueur(joueurEnCours);
                    } else {
                        $("#result").html(`Victore de ${joueurEnCours}`)
                        let scoreAMarquer = `score${joueurEnCours}`;
                        let score;
                        if ("scoreX" === scoreAMarquer) {
                            scoreX++;
                            score = scoreX;
                        } else if ("scoreO" === scoreAMarquer) {
                            scoreO++;
                            score = scoreO;
                        }
                        $(`#${scoreAMarquer}`).html(score)
                        play = false;
                    }
                }
            }
        }

        function changeJoueur(joueurEnCours) {
            switch (joueurEnCours) {
                case joueurX:
                    return joueurO;
                case joueurO:
                    return joueurX;
            }
        }

        function checkWin(symbole) {
            if (
                $("#1").html() === symbole &&
                $("#2").html() === symbole &&
                $("#3").html() === symbole
            ) {
                return true
            } else if (
                $("#4").html() === symbole &&
                $("#5").html() === symbole &&
                $("#6").html() === symbole
            ) {
                return true
            } else if (
                $("#7").html() === symbole &&
                $("#8").html() === symbole &&
                $("#9").html() === symbole
            ) {
                return true
            } else if (
                $("#1").html() === symbole &&
                $("#4").html() === symbole &&
                $("#7").html() === symbole
            ) {
                return true
            } else if (
                $("#2").html() === symbole &&
                $("#5").html() === symbole &&
                $("#8").html() === symbole
            ) {
                return true
            } else if (
                $("#3").html() === symbole &&
                $("#6").html() === symbole &&
                $("#9").html() === symbole
            ) {
                return true
            } else if (
                $("#1").html() === symbole &&
                $("#5").html() === symbole &&
                $("#9").html() === symbole
            ) {
                return true
            } else if (
                $("#3").html() === symbole &&
                $("#5").html() === symbole &&
                $("#7").html() === symbole
            ) {
                return true;
            } else {
                return false;
            }
        }

        $("#reset").click(reset);

        function reset() {
            for (let i = 1; i < 10; i++) {
                $(`#${i}`).html("")
            }
            $("#result").html("")
            play = true;
            randomPlayer();
        }
    }
)