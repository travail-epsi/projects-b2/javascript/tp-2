$(document).ready(function () {
    let currentEntry;
    let previousEntry = "";
    let result;
    let a, op;
    $(".btn").click(onClick);

    function onClick() {
        currentEntry = $(this).html();
        console.log(currentEntry);
        result = currentEntry
        if (!isNumber(currentEntry)) { //Si ce n'est pas un nombre
            if (!isOperator(currentEntry)) {//Si ce n'est un opérateur
                if (!isEqual(currentEntry)) {//Si ce n'est pas le égal
                    previousEntry = "";
                    result = 0;//Alors c'est le reset
                } else {//Si c'est le égal
                    switch (op) {
                        case "+" :
                            result = addition(a, previousEntry);
                            break;
                        case "-":
                            result = soustraction(a, previousEntry);
                            break;
                        case '*':
                            result = multiplication(a, previousEntry);
                            break;
                        case "/":
                            result = division(a, previousEntry);
                            break;
                    }
                    previousEntry = result;
                }
            } else {//Si c'est un opérateur
                op = currentEntry;
                a = previousEntry;
                previousEntry = "";
            }
        } else {//Si c'est un nombre
            result = previousEntry + currentEntry;
            previousEntry = result;
        }

        update(result);
    }


    function multiplication(param1, param2) {
        return param1 * param2;
    }

    function division(param1, param2) {
        return param1 / param2;
    }

    function addition(param1, param2) {
        return parseInt(param1) + parseInt(param2);
    }

    function soustraction(param1, param2) {
        return param1 - param2;
    }

    function isNumber(currentKey) {
        return !isNaN(currentKey);
    }

    function isOperator(currentKey) {
        return currentKey == "/" || currentKey == "*" || currentKey == "+" || currentKey == "-";
    }

    function isEqual(currentKey) {
        return currentKey == "=";
    }

    function update(result) {
        $("#result").html(result.toString())
    }
});

